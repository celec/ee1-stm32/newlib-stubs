#include <stdio.h>
#include "communications.h" // send_char(), send_short()
#include "uart.h" // contains uart_*.c prototypes

int main(void) {
	int msk = (1 << 11) | (1 << 12) | (1 << 13);
	char newline[3] = "\r\n\0";
    long k;

	clock_setup();
	init_gpio();
	usart_setup();

	while(1){
		led_set(msk);
		delay(100);
        
		my_putchar('m');
        printf("%c", 'p');
        fflush(0);

		led_clr(msk);
    }
	
	return 0;
}
